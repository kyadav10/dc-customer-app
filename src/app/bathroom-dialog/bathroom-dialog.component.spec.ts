import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BathroomDialogComponent } from './bathroom-dialog.component';

describe('BathroomDialogComponent', () => {
  let component: BathroomDialogComponent;
  let fixture: ComponentFixture<BathroomDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BathroomDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BathroomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
